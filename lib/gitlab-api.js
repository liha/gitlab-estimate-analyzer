var rp = require('request-promise');
var _ = require('lodash');
var config = require('config');
var Promise = require('bluebird');

var apiUrl = 'https://gitlab.com/api/v4';

function getProjects(search) {
    return Promise
        .resolve(config)
        .then(config => {
            if(!config.token) {
                throw new Error('noToken')
            }
            return config;
        })
        .then(config => {
            return {
                uri: `${apiUrl}/projects`,
                qs: {
                    membership: true,
                    search: search
                },
                headers: {
                    'Private-Token': config.token
                },
                json: true
            };
        })
        .then(options => {
            return rp(options);
        });
}

function getUsers(username) {
    return Promise
        .resolve(config)
        .then(config => {
            if(!config.token) {
                throw new Error('noToken')
            }
            return config;
        })
        .then(config => {
            return {
                uri: `${apiUrl}/users`,
                qs: {
                    username: username
                },
                headers: {
                    'Private-Token': config.token
                },
                json: true
            };
        })
        .then(options => {
            return rp(options);
        });
}

function getEstimate(projectId, options) {
    options = options || {};
    options.per_page = 100;
    return Promise
        .resolve(config)
        .then(config => {
            if(!config.token) {
                throw new Error('noToken')
            }
            if(!projectId) {
                throw new Error('noProjectId');
            }
            return config;
        })
        .then(config => {
            return {
                uri: `${apiUrl}/projects/${projectId}/issues`,
                qs: options,
                headers: {
                    'Private-Token': config.token
                },
                json: true
            };
        })
        .then(options => {
            return rp(options);
        })
        .then(issues => {
            var timeStats = {
                coverage: 0,
                estimate: 0,
                spend: 0,
                issuesCount: issues.length,
                estimatesCount: 0
            }
            _.forEach(issues, issue => {
                if(issue.time_stats && issue.time_stats.time_estimate > 0) {
                    timeStats.estimatesCount = timeStats.estimatesCount + 1;
                    timeStats.estimate = timeStats.estimate + issue.time_stats.time_estimate;
                    timeStats.spend = timeStats.spend + issue.time_stats.total_time_spent;             
                }
            })
            timeStats.coverage = _.round(timeStats.estimatesCount / timeStats.issuesCount * 100);
            return timeStats;
        });
}

function getMilestones(projectId) {
    return Promise
        .resolve(config)
        .then(config => {
            if(!config.token) {
                throw new Error('noToken')
            }
            if(!projectId) {
                throw new Error('noProjectId');
            }
            return config;
        })
        .then(config => {
            return {
                uri: `${apiUrl}/projects/${projectId}/milestones`,
                headers: {
                    'Private-Token': config.token
                },
                json: true
            };
        })
        .then(options => {
            return rp(options);
        });
}

exports.getProjects = getProjects;
exports.getEstimate = getEstimate;
exports.getMilestones = getMilestones;
exports.getUsers = getUsers;
