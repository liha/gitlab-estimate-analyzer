module.exports = function(err) {
    if(err.message == 'noToken') {
        console.log(' ');
        console.log('No token was found in config, run the following command to fix this');
        console.log(' ');
        console.log('gitlab-estimate-analyzer auth setup <token>');
        console.log(' ');
    } else if(err.message == 'noProjectId') {
        console.log(' ');
        console.log('No project id was provided, you can get your project id with this command');
        console.log(' ');
        console.log('gitlab-estimate-analyzer project list [search]');
        console.log(' ');      
    } else {
        conosle.log('Something went wrong: ' + err.message);
    }
};
