# Gitlab estimate analyzer

A small cli tool written in NodeJS to analyze time estimates in Gitlab.

## Install

`$ npm install -g gitlab-estimate-analyzer`

## Usage

`$ gitlab-estimate-analyzer --help`

*Please note*

If config file is empty the following message will be printed:

```
WARNING: No configurations found in configuration directory:/Users/JohanAsplund/projects/gitlab-estimate-analyzer/config
WARNING: To disable this warning set SUPPRESS_NO_CONFIG_WARNING in the environment.
```

This is normal behavior

## Examples

### List projects

`$ gitlab-estimate-analyzer projects-list`