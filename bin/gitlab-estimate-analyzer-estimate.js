#!/usr/bin/env node
var program = require('commander');
var _ = require('lodash');

var api = require('../lib/gitlab-api');

program
    .command('milestone <project> <milestone> [fee]')
    .description('Get estimate for a milestone')
    .action((project, milestone, fee, program) => {
        api.getEstimate(project, { milestone: milestone })
            .then(timeEstimate => {
                console.log(' ');
                console.log(`Estimate coverage: ${timeEstimate.coverage}% (based on ${timeEstimate.issuesCount} issues)`)
                console.log(`   Total Estimate: ${_.round(timeEstimate.estimate / 60 / 60)}h (${_.round(timeEstimate.estimate / 60 / 60 / 8, 1)} day(s))`);
                console.log(`      Total Spent: ${_.round(timeEstimate.spend / 60 / 60)}h (${_.round(timeEstimate.spend / 60 / 60 / 8, 1)} day(s))`);
                if(fee) {
                    console.log(`   Estimated cost: ${_.round((timeEstimate.estimate / 60 / 60) * fee)} SEK`);
                }
                console.log(' ');
            })
            .catch(err => {
                console.error('Something went wrong');
            });
    });

    program
    .command('assignee <project> <assigneeId> [fee]')
    .description('Get estimate for a assignee')
    .action((project, assigneeId, fee, program) => {
        api.getEstimate(project, { assignee_id: assigneeId })
            .then(timeEstimate => {
                console.log(' ');
                console.log(`Estimate coverage: ${timeEstimate.coverage}% (based on ${timeEstimate.issuesCount} issues)`)
                console.log(`   Total Estimate: ${_.round(timeEstimate.estimate / 60 / 60)}h (${_.round(timeEstimate.estimate / 60 / 60 / 8, 1)} day(s))`);
                console.log(`      Total Spent: ${_.round(timeEstimate.spend / 60 / 60)}h (${_.round(timeEstimate.spend / 60 / 60 / 8, 1)} day(s))`);
                if(fee) {
                    console.log(`   Estimated cost: ${_.round((timeEstimate.estimate / 60 / 60) * fee)} SEK`);
                }
                console.log(' ');
            })
            .catch(err => {
                console.error('Something went wrong');
            });
    });

program.parse(process.argv);

// show help if no command was provided
if(program.args.length === 0) {
    program.help();
}
