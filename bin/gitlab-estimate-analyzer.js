#!/usr/bin/env node
var path = require('path');
process.env.NODE_CONFIG_DIR = path.resolve(__dirname, '..', 'config');

var config = require('config');
var program = require('commander');
var package = require('../package');

program
    .version(package.version)
    .description(package.description);

program.command('auth', 'handle authentication');
program.command('projects', 'Handle project related stuff');
program.command('users', 'Handle users related stuff');
program.command('estimate', 'Handle project estimates');
program
    .command('config')
    .description('Show config file')
    .action(program => {
        console.log(JSON.stringify(config, null, 2));
    });

program.parse(process.argv);

// show help if no command was provided
if(program.args.length === 0) {
    program.help();
}
