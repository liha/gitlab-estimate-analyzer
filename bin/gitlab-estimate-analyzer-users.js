#!/usr/bin/env node
var program = require('commander');
var package = require('../package');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var Table = require('cli-table');

var api = require('../lib/gitlab-api');
var errorHandler = require('../lib/error-handler');

program
    .command('list [search]')
    .description('List all your users')
    .action((search, program) => {
        api.getUsers(search)
            .then(users => {
                console.log(`Found ${users.length} users`);
                if(users.length > 0) {
                    var table = new Table({
                        head: ['Username', 'Name', 'ID'],
                        colWidths: [40, 40, 20]
                    });                  
                    _.forEach(users, user => {
                        table.push([user.username, user.name, user.id]);
                    });
                }
                console.log(table.toString());
            })
            .catch(errorHandler);
    });

program.parse(process.argv);

// show help if no command was provided
if(program.args.length === 0) {
    program.help();
}
