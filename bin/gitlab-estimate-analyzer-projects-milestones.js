#!/usr/bin/env node
var program = require('commander');
var _ = require('lodash');
var Table = require('cli-table');

var api = require('../lib/gitlab-api');

program
    .command('list <project>')
    .description('List all your project milestones')
    .action((project, program) => {
        api.getMilestones(project)
            .then(milestones => {
                console.log(`Found ${milestones.length} milestones`);
                if(milestones.length > 0) {
                    var table = new Table({
                        head: ['Project', 'State'],
                        colWidths: [60, 20]
                    });                  
                    _.forEach(milestones, milestone => {
                        table.push([milestone.title, milestone.state]);
                    });
                }
                console.log(table.toString());
            })
            .catch(err => {
                console.error('Something went wrong');
            });
    });

program.parse(process.argv);

// show help if no command was provided
if(program.args.length === 0) {
    program.help();
}
