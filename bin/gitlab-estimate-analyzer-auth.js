#!/usr/bin/env node
var config = require('config');
var program = require('commander');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');

var localConfigFile = path.resolve(__dirname, '../', 'config', 'local.json');

program
    .command('setup [token]')
    .description('Setup credentials for gitlab access')
    .action((token, program) => {
        var configFile = _.cloneDeep(config);
        configFile.token = token;
        
        fs.writeFileSync(localConfigFile, JSON.stringify(configFile, null, 2));
        console.log('Config written to path: ' + localConfigFile);
    });

program
    .command('destroy')
    .description('delete token from config')
    .action((token, program) => {
        delete config.token;
        fs.writeFileSync(localConfigFile, JSON.stringify(config, null, 2));
        console.log('Config written to path: ' + localConfigFile);
    });

program.parse(process.argv);

// show help if no command was provided
if(program.args.length === 0) {
    program.help();
}
