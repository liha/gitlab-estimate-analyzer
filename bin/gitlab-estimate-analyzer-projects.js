#!/usr/bin/env node
var program = require('commander');
var package = require('../package');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var Table = require('cli-table');

var api = require('../lib/gitlab-api');
var errorHandler = require('../lib/error-handler');

program
    .command('list [search]')
    .description('List all your projects')
    .action((search, program) => {
        api.getProjects(search)
            .then(projects => {
                console.log(`Found ${projects.length} projects`);
                if(projects.length > 0) {
                    var table = new Table({
                        head: ['Project', 'id'],
                        colWidths: [60, 20]
                    });                  
                    _.forEach(projects, project => {
                        table.push([project.name, project.id]);
                    });
                }
                console.log(table.toString());
            })
            .catch(errorHandler);
    });

program.command('milestones', 'Handle project milestones');

program.parse(process.argv);

// show help if no command was provided
if(program.args.length === 0) {
    program.help();
}
